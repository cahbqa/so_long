/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kmumm <kmumm@student.21-school.ru>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/30 20:26:18 by kmumm             #+#    #+#             */
/*   Updated: 2022/02/01 23:13:06 by kmumm            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils/utils.h"

int	ft_render(t_game *game)
{
	mlx_clear_window(game->mlx, game->win);
	ft_draw_map(game);
	return (0);
}

int	key_handler(int key, t_game *game)
{
	if (key == 53)
		close_event(throw_error(game, 1));
	if (key == 13 || key == 126)
		player_move(game, UP);
	if (key == 0 || key == 123)
		player_move(game, LEFT);
	if (key == 1 || key == 125)
		player_move(game, DOWN);
	if (key == 2 || key == 124)
		player_move(game, RIGHT);
	ft_render(game);
	return (0);
}

void	game_init(t_game *game, int argc, char **argv)
{
	game->player = malloc(sizeof(t_player));
	check_arg(argc, argv, game);
	read_map(argv, game);
	check_map(game);
	game->tile_size = 24;
	game->player->moves = 0;
	game->error_code = 1;
	printf("MOVE: 0\n");
	game->mlx = mlx_init();
	game->win = mlx_new_window(game->mlx, game->tile_size * game->height,
			game->tile_size * game->width, "MYWIN");
	ft_init_pics(game);
	ft_render(game);
}

void	win(t_game *game)
{
	
}

int	main(int argc, char **argv)
{
	t_game	game;
	t_list	*start;

	game_init(&game, argc, argv);
	mlx_key_hook(game.win, key_handler, &game);
	mlx_hook(game.win, 17, 0, close_event, &game);
	mlx_loop(game.mlx);
	return (0);
}
