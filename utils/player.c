/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kmumm <kmumm@student.21-school.ru>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/01 20:30:29 by kmumm             #+#    #+#             */
/*   Updated: 2022/02/01 23:25:28 by kmumm            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

void	ft_action(t_game *g, int x, int y, int ACT_TYPE)
{
	if (ACT_TYPE == MOVE || ACT_TYPE == COLLECT_ACT)
	{
		set_type_by_coors(g, y, x, 'P');
		set_type_by_coors(g, g->player->coors[1], g->player->coors[0], '0');
		g->player->coors[0] = x;
		g->player->coors[1] = y;
		++g->player->moves;
		printf("MOVES: %d\n", g->player->moves);
	}
	if (ACT_TYPE == COLLECT_ACT)
		++g->player->score;
	if (ACT_TYPE == EXIT_ACT && g->player->score == g->nums[0])
	{
		set_type_by_coors(g, y, x, 'P');
		set_type_by_coors(g, g->player->coors[1], g->player->coors[0], '0');
		printf("!!!win!!!\n");
	}
}

void	player_move(t_game *g, int direction)
{
	char		type;
	t_player	*player;
	int			dest[2];

	player = g->player;
	dest[0] = player->coors[0] + direction / 2;
	dest[1] = player->coors[1] + direction % 2;
	type = get_type_by_coors(g, dest[1], dest[0]);
	if (type == -1)
		return ;
	if (type == '0')
		ft_action(g, dest[0], dest[1], MOVE);
	if (type == 'C')
		ft_action(g, dest[0], dest[1], COLLECT_ACT);
	if (type == 'E')
		ft_action(g, dest[0], dest[1], EXIT_ACT);
}
