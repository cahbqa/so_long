# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: kmumm <kmumm@student.21-school.ru>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/01/30 19:57:03 by kmumm             #+#    #+#              #
#    Updated: 2022/02/01 23:00:24 by kmumm            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

UTILS = utils/get_next_line.c\
		utils/get_next_line_utils.c\
		utils/list.c\
		utils/map.c\
		utils/errors_handlers.c\
		utils/pic_draw.c\
		utils/player.c

UTILS_OBJS = $(UTILS:.c=.o)

#UTILS_HEADER = utils/utils.h

#LIB_PATH = utils/lib.a

#GREEN = \033[0;32m
#RED = \033[0;31m
#RESET = \033[0m

#lib: $(UTILS_OBJS) $(UTILS_HEADER)
#	ar rcs $(LIB_PATH) $?
#	#ranlib $(LIB_PATH)

all:
	cc main.c $(UTILS) -L ./minilibx/ -lmlx -I ./minilibx/ -o check -I utils/utils.h -framework OpenGL -framework AppKit